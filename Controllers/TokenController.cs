﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Glossary.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TokenController : ControllerBase
    {
        [HttpGet]
        public ActionResult<IRestResponse> CreateToken()
        {
            var client = new RestClient("https://test-codan-no.eu.auth0.com/oauth/token");
            var request = new RestRequest(Method.POST);

            request.AddHeader("content-type", "application/json");
            request.AddParameter("application/json", "{\"client_id\":\"NiBYyu9yS5BI63g9Lz2MxxhAUQC9ika7\",\"client_secret\":\"yjUtrHJu1cQf6AGeFSOKOpUCzz8RPHNcfPUBhP0E_7JjywmcsQoutrwu_0Hy-YQ9\",\"audience\":\"https://tiatechnology.com/spd/sales\",\"grant_type\":\"client_credentials\", \"scope\":\"sales:get_salesproducts_by_channel\", \"scope\":\"sales:get_premium\", \"scope\":\"sales:new_salespolicy\", \"scope\":\"read:channel\"}", ParameterType.RequestBody);

            IRestResponse response = client.Execute(request);

            return Ok(response.Content);
        }
    }
}
